def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string,number=2)
  repeated = ""
  number.times do
    repeated += "#{string} "
  end
  repeated.strip
end

def start_of_word(word,letters)
  word.slice(0,letters)
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(sentence)
  little_words = ['and','the','over']
  titleized = sentence.split.map.with_index(0) do |word, idx|
    if little_words.include?(word) && idx > 0
      word
    else
      word.capitalize
    end 
  end
  titleized.join(' ')
end
