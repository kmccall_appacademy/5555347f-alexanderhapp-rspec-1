VOWELS = ['a','e','i','o','u']

def first_vowel_index(word)
  word.chars.index {|c| VOWELS.include?(c)}
end

def first_consonants(word)
  if word.slice(0,2) == "qu"
    word.slice(0,2)
  elsif VOWELS.include?(word[0]) == false && word.slice(1,2) == "qu"
    word.slice(0,3)
  else
    word.slice(0,first_vowel_index(word))
  end
end

def translate_word(word)
  start_index = first_consonants(word).length
  word.slice(start_index,word.length - start_index) + first_consonants(word) + "ay"
end

def translate(sentence)
  sentence.split.map {|word| translate_word(word)}.join(' ')
end
