def add(a,b)
  a + b
end

def subtract(a,b)
  a - b
end

def sum(params)
  params.inject(0, :+)
end

def multiply(*args)
  args.inject(:*)
end

def power(a,b)
  a**b
end

def factorial(number)
  number > 0 ? (1..number).inject(:*) : 1 
end
